// app.js

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

import HomeComponent from './components/HomeComponent.vue';
import CreateComponent from './components/CreateComponent.vue';
import IndexComponent from './components/IndexComponent.vue';
import EditComponent from './components/EditComponent.vue';
import ShowComponent from './components/ShowComponent.vue';

const routes = [
    {
        name: 'create',
        path: '/',
        component: CreateComponent 
    },
    {
        name: 'home',
        path: '/thanks',
        component: HomeComponent
    },
    {
        name: 'posts',
        path: '/posts',
        component: IndexComponent
    },
    {
        name: 'show',
        path: '/show/:id',
        component: ShowComponent
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditComponent
    }
    
];

const router = new VueRouter({ mode: 'history', routes: routes });
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');