<?php

namespace App\Listeners;

use Mail;

use App\Events\UserPostReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserPostWelcomeEmail 
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserPostReceived  $event
     * @return void
     */
    public function handle(UserPostReceived $event)
    {

        $data = array('first_name' => $event->post->first_name, 'last_name' => $event->post->last_name,
            'email' => $event->post->email, 'phone_number' => $event->post->phone_number,
            'address_one' => $event->post->address_one, 'address_two' => $event->post->address_two, 
            'city_town' => $event->post->city_town,'province' => $event->post->province,
            'postal_code' => $event->post->postal_code, 'question' => $event->post->question, 
            'email_opt' => $event->post->email_opt);

        Mail::send('emails.user_details', $data, function($message)  use ($data) {
            $message->to('jaahvicky@gmail.com')
                ->subject('User Post Details Below');
            $message->from($data['email']);
        });
    }
}
