<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [ 'first_name', 'last_name', 'email', 
    'phone_number', 'address_one', 'address_two', 'city_town',
     'province', 'postal_code', 'question', 'email_opt' ];
}
